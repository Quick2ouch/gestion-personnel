/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App.ORM;

import App.Models.Paiement;
import java.sql.PreparedStatement;
import App.Models.Employe;
import App.Models.Categorie;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Geek-Dev
 */
public class PaiementORM extends ORM<Paiement>{
    public PaiementORM() {}
    
    public Paiement find(int id){
        Paiement pa = new Paiement();
        
          try {
            PreparedStatement ps = con.prepareStatement("select pay.*,e.* from paiement as pay left join employe e on e.idEmploye=pay.idEmploye where pay.idpaiement=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                pa = new Paiement(
                        rs.getInt("idpaiement"),
                        new Date(rs.getDate("date_paiement").getTime()),
                        rs.getFloat("montant"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        )
                );
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pa;
    }

   

    @Override
    public Paiement create(Paiement obj) {
        try {
            PreparedStatement ps = con.prepareStatement("insert into paiement(date_paiement,montant,idEmploye) values(?,?,?)");
            ps.setDate(1, new Date(obj.getDatePaiement().getTime()));
            ps.setFloat(2, obj.getMontant());
            ps.setInt(3, obj.getEmploye().getIdEmploye());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Paiement update(Paiement obj) {
        try {
            PreparedStatement ps = con.prepareStatement("update paiement set date_paiement=? where idPaiement=?");
            ps.setDate(1, new Date(obj.getDatePaiement().getTime()));
            ps.setFloat(2, obj.getIdPaiement());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement ps = con.prepareStatement("delete from  paiement where idpaiement=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    
    /**
     * Retourner la liste des paiement d'un certain employé
     * @param id
     * @return 
     */
    
    public Vector<Paiement>  listByEmploye(int id) {
                Vector<Paiement>  vv = new Vector<Paiement> ();
        try {
            PreparedStatement ps = con.prepareStatement("select pay.*,e.* from paiement as pay left join employe e on e.idEmploye=pay.idEmploye where pay.idEmploye=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               vv.add(new Paiement(
                        rs.getInt("idpaiement"),
                        new Date(rs.getDate("date_paiement").getTime()),
                        rs.getFloat("montant"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        )
                ));
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vv;
    }
    
     @Override
    public Vector<Paiement> list() {
        Vector<Paiement> vv = new Vector<Paiement>();
       try {
            PreparedStatement ps = con.prepareStatement("select pay.*,e.* from paiement as pay left join employe e on e.idEmploye=pay.idEmploye order by pay.date_paiement asc, e.Nom asc");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            vv.add(new Paiement(
                        rs.getInt("idpaiement"),
                        new Date(rs.getDate("date_paiement").getTime()),
                        rs.getFloat("montant"),
                        new Employe(
                            rs.getInt("idEmploye"),
                                rs.getString("Nom"),
                                rs.getString("Prenom"),
                                new Date(rs.getDate("DateNaissance").getTime()),
                                new Date(rs.getDate("DateEntree").getTime()),
                                new Date(rs.getDate("DateSortie").getTime()),
                                new Categorie()
                        )
                ));
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vv;
    }

  
}
