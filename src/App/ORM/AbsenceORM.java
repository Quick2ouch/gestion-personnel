/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App.ORM;

import App.Models.Absence;
import App.Models.Categorie;
import App.Models.Conge;
import App.Models.Employe;
import System.LogOperation;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author EL OMRI
 */
public class AbsenceORM extends ORM<Absence>{

    public AbsenceORM() {
    }

    @Override
    public Absence find(int id) {
        Absence ab = new Absence();
        try {
            PreparedStatement ps = con.prepareStatement("select a.*,e.* from absence as a left join employe e on e.idEmploye=a.idEmploye where a.idAbsence=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Employe emp = new Employe();
                emp.setIdEmploye(rs.getInt("idEmploye"));
                emp.setNom(rs.getString("Nom"));
                emp.setPrenom(rs.getString("Prenom"));
                emp.setDateNaissance(new Date(rs.getDate("DateNaissance").getTime()));
                emp.setDateEntree(new Date(rs.getDate("DateEntree").getTime()));
                emp.setDateSortie(new Date(rs.getDate("DateSortie").getTime()));
                ab.setidAbsence(rs.getInt("idAbsence"));
                ab.setDebut(new Date(rs.getDate("Debut").getTime()));
                ab.setFin(new Date(rs.getDate("Fin").getTime()));
                ab.setMotif(rs.getString("Motif"));
                ab.setJustificatif(rs.getString("Justificatif"));
                ab.setEmploye(emp);
            }
        } catch (Exception e) {
        }
        return ab;
    }

    @Override
    public Vector<Absence> list() {
        Vector<Absence> vab = new Vector<Absence>();
        try {
            PreparedStatement ps = con.prepareStatement("select a.*,e.* from absence as a left join employe e on e.idEmploye=a.idEmploye");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Absence ab = new Absence();
                Employe emp = new Employe();
                emp.setIdEmploye(rs.getInt("idEmploye"));
                emp.setNom(rs.getString("Nom"));
                emp.setPrenom(rs.getString("Prenom"));
                emp.setDateNaissance(new Date(rs.getDate("DateNaissance").getTime()));
                emp.setDateEntree(new Date(rs.getDate("DateEntree").getTime()));
                emp.setDateSortie(new Date(rs.getDate("DateSortie").getTime()));
                ab.setidAbsence(rs.getInt("idAbsence"));
                ab.setDebut(new Date(rs.getDate("Debut").getTime()));
                ab.setFin(new Date(rs.getDate("Fin").getTime()));
                ab.setMotif(rs.getString("Motif"));
                ab.setJustificatif(rs.getString("Justificatif"));
                ab.setEmploye(emp);
                vab.add(ab);
            }
        } catch (Exception e) {
        }
        return vab;
    }

    @Override
    public Absence create(Absence obj) {
        try {
            PreparedStatement ps = con.prepareStatement("insert into absence(Debut,Fin,Motif,Justificatif,idEmploye) values(?,?,?,?,?)");
            ps.setDate(1, new Date(obj.getDebut().getTime()));
            ps.setDate(2, new Date(obj.getFin().getTime()));
            ps.setString(3, obj.getMotif());
            ps.setString(4, obj.getJustificatif());
            ps.setInt(5, obj.getEmploye().getIdEmploye());
            ps.executeUpdate();
            ps.close();
            new LogOperation("Ajout absence pour "+obj.getEmploye().getIdEmploye());
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Absence update(Absence obj) {
        try {
            PreparedStatement ps = con.prepareStatement("update absence set Debut=?, Fin=?, Motif=?, Justificatif=?, idEmploye=? where idAbsence=?");
            ps.setDate(1, new Date(obj.getDebut().getTime()));
            ps.setDate(2, new Date(obj.getFin().getTime()));
            ps.setString(3, obj.getMotif());
            ps.setString(4, obj.getJustificatif());
            ps.setInt(5, obj.getEmploye().getIdEmploye());
            ps.setInt(6, obj.getidAbsence());
            ps.executeUpdate();
            ps.close();
            new LogOperation("Mise a jour absence "+obj.getidAbsence());
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement ps = con.prepareStatement("delete from absence where idAbsence=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
            new LogOperation("Suppression absence "+id);
        } catch (Exception e) {
        }
    }
    
}
