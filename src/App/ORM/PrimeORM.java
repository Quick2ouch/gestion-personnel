/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.ORM;

import App.Models.Employe;
import App.Models.Prime;
import System.LogOperation;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;


/**
 *
 * @author Naima
 */
public class PrimeORM extends ORM<Prime>{
    static int kil =0;
    @Override
    public Prime find(int id) {
        Prime pm = new Prime();
        try {
            PreparedStatement ps = con.prepareStatement("select p.*,e.* from prime as p left join employe e on e.idEmploye=p.idEmploye where p.idPrime=? ");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Employe emp = new Employe();
                emp.setIdEmploye(rs.getInt("idEmploye"));
                emp.setNom(rs.getString("Nom"));
                emp.setPrenom(rs.getString("Prenom"));
                emp.setDateNaissance(new Date(rs.getDate("DateNaissance").getTime()));
                emp.setDateEntree(new Date(rs.getDate("DateEntree").getTime()));
                emp.setDateSortie(new Date(rs.getDate("DateSortie").getTime()));
                pm.setIdPrime(rs.getInt("idPrime"));
                pm.setType(rs.getString("type"));
                pm.setMontantprime(rs.getFloat("MontantPrime"));
                pm.setDateprime(new Date(rs.getDate("DatePrime").getTime()));
                pm.setEmploye(emp);
            }
        } catch (Exception e) {
        }
        return pm;
    }

    @Override
    public Vector<Prime> list() {
        Vector<Prime> vpm = new Vector<Prime>();
        try {
            PreparedStatement ps = con.prepareStatement("select p.*,e.* from prime as p left join employe e on e.idEmploye=p.idEmploye");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Prime pm = new Prime();
                Employe emp = new Employe();
                emp.setIdEmploye(rs.getInt("idEmploye"));
                emp.setNom(rs.getString("Nom"));
                emp.setPrenom(rs.getString("Prenom"));
                emp.setDateNaissance(new Date(rs.getDate("DateNaissance").getTime()));
                emp.setDateEntree(new Date(rs.getDate("DateEntree").getTime()));
                emp.setDateSortie(new Date(rs.getDate("DateSortie").getTime()));
                pm.setIdPrime(rs.getInt("idPrime"));
                pm.setType(rs.getString("type"));
                pm.setMontantprime(rs.getFloat("MontantPrime"));
                pm.setDateprime(new Date(rs.getDate("DatePrime").getTime()));
                pm.setEmploye(emp);
                vpm.add(pm);
            }
        } catch (Exception e) {
        }
        return vpm;    }

    @Override
    public Prime create(Prime obj) {
        try {
            PreparedStatement ps = con.prepareStatement("insert into prime(type,MontantPrime,DatePrime,idEmploye) values(?,?,?,?)");
            ps.setString(1, obj.getType());
            ps.setFloat(2, obj.getMontantprime());
            ps.setDate(3, new Date(obj.getDateprime().getTime()));
            ps.setInt(4, obj.getEmploye().getIdEmploye());
            ps.executeUpdate();
            ps.close();
            new LogOperation("Ajout prime "+obj.getMontantprime()+" pour" +obj.getEmploye().getNom() + " "+ obj.getEmploye().getPrenom());
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Prime update(Prime obj) {
        try {
            PreparedStatement ps = con.prepareStatement("update prime set type=?, MontantPrime=?, DatePrime=?, idEmploye=? where idPrime=?");
            ps.setString(1, obj.getType());
            ps.setFloat(2, obj.getMontantprime());
            ps.setDate(3, new Date(obj.getDateprime().getTime()));
            ps.setInt(4, obj.getEmploye().getIdEmploye());
            ps.setInt(5, obj.getIdPrime());
            ps.executeUpdate();
            ps.close();
            new LogOperation("Mise a jour prime "+obj.getIdPrime());
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement ps = con.prepareStatement("delete from prime where idPrime=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
            new LogOperation("Suppression prime "+id);
        } catch (Exception e) {
        }
    }
    
}