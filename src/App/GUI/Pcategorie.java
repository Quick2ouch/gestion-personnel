/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.GUI;

import App.Models.Categorie;
import App.ORM.CategorieORM;
import App.Tab.CategorieTab;
import System.Bootstrap;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Hamza
 */
public class Pcategorie extends javax.swing.JPanel {
    CategorieORM catorm;
    CategorieTab emtb;
    Bootstrap app = Bootstrap.getInstance();
    
    /**
     * Creates new form Pcategorie
     */
    public Pcategorie() {
        catorm = new CategorieORM();
        initComponents();
        goo();
        affiche();
    }
    
    public void affiche(){
        initComponents();
        catorm = new CategorieORM();
        emtb = new CategorieTab();
        Categorie em = new Categorie();
        templ.setModel(emtb);
        Vector<Categorie> vms=catorm.list();
        emtb.setdata(vms);
        templ.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt){
                TablMouseClicked();
            }
        });
        templ.getColumnModel().getColumn(0).setPreferredWidth(20);
        templ.getColumnModel().getColumn(0).setMaxWidth(20);
        templ.getColumnModel().getColumn(1).setPreferredWidth(35);
        templ.getColumnModel().getColumn(1).setMaxWidth(35);
        templ.getColumnModel().getColumn(4).setPreferredWidth(90);
        templ.getColumnModel().getColumn(4).setMaxWidth(90);
        templ.getColumnModel().getColumn(5).setPreferredWidth(90);
        templ.getColumnModel().getColumn(5).setMaxWidth(90);
        templ.getColumnModel().getColumn(6).setPreferredWidth(90);
        templ.getColumnModel().getColumn(6).setMaxWidth(90);
    }
    
    private void TablMouseClicked() {                                           
        // TODO add your handling code here:
        int i = templ.getSelectedRow();
        int j = templ.getSelectedColumn();
        if(j==6){
            this.removeAll();
            this.revalidate();
            this.repaint();
            Categorie e = new Categorie();
            e.setIdCategotrie((int)templ.getValueAt(i, 1));
            e.setIntituleCategorie((String)templ.getValueAt(i, 2));
            e.setSalaire((float)templ.getValueAt(i, 3));
            e.setJourConge((int)templ.getValueAt(i, 4));
            e.setPenalite((float)templ.getValueAt(i, 5));
            
            bajoutcomp("MODIFIER UNE CATÉGORIE", e);
        }
    }
    
    
    
    public void goo(){
        lajout = new JLabel();
        lintitule = new JLabel("Intitulé :");
        lsalaire = new JLabel("Salaire :");
        ljour_conge = new JLabel("Nombre de jours de congé :");
        lpenalite = new JLabel("Penalié :");
        
        tintitule = new JTextField();
        tsalaire = new JTextField();
        tjour_conge = new JTextField();
        tpenalite = new JTextField();
        
        
        bcreer = new JButton("Créer");
        bmodifier = new JButton("Modifier");
        bback = new JButton("Annuler");
        
        bcreer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcreerActionPerformed(evt);
            }
        });
        bmodifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bmodifierActionPerformed(evt);
            }
        });
        bback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbackActionPerformed(evt);
            }
        });
    }
    
    private void bcreerActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        this.removeAll();
        this.revalidate();
        this.repaint();

        catorm.create(
                new Categorie(
                        Float.parseFloat(tsalaire.getText()), 
                        Integer.parseInt(tjour_conge.getText()), 
                        tintitule.getText(), 
                        Float.parseFloat(tpenalite.getText())
                )
        );
        
        //emorm.create(new Employe(tnom.getText(), tprenom.getText(), dcnaiss.getDate(), dcentree.getDate(), dcsortie.getDate(),(Categorie) ccateg.getSelectedItem()));
        affiche();
    } 
    
    private void bmodifierActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        this.removeAll();
        this.revalidate();
        this.repaint();
        
        catorm.update(
                new Categorie(
                        id,
                        Float.parseFloat(tsalaire.getText()), 
                        Integer.parseInt(tjour_conge.getText()), 
                        tintitule.getText(), 
                        Float.parseFloat(tpenalite.getText())
                )
        );
        //emorm.update(new Employe(id, tnom.getText(), tprenom.getText(), dcnaiss.getDate(), dcentree.getDate(), dcsortie.getDate(),(Categorie) ccateg.getSelectedItem()));
        affiche();
    }
    
    private void bbackActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        this.removeAll();
        this.revalidate();
        this.repaint();
        affiche();
    } 

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        templ = new javax.swing.JTable();
        btnAjouter = new javax.swing.JButton();
        bsupprimer = new javax.swing.JButton();

        templ.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(templ);

        btnAjouter.setText("Ajouter une catégorie");
        btnAjouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjouterActionPerformed(evt);
            }
        });

        bsupprimer.setBackground(new java.awt.Color(255, 102, 102));
        bsupprimer.setText("Supprimer");
        bsupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bsupprimerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 748, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bsupprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnAjouter)
                            .addGap(569, 569, 569))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(142, Short.MAX_VALUE)
                .addComponent(btnAjouter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bsupprimer)
                .addGap(54, 54, 54))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bsupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bsupprimerActionPerformed
        // TODO add your handling code here:

        int t = 0 ;
        for(int i=0;i<templ.getRowCount();i++)
        if((boolean)templ.getValueAt(i, 0)) t++;
        if(t>0)
        if (JOptionPane.showConfirmDialog(null, "Voulez vous vraiment supprimer "+t+" catégorie(s)?", "WARNING",
            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        for(int i=0;i<templ.getRowCount();i++)
        if((boolean)templ.getValueAt(i, 0)){
            catorm.delete((int)templ.getValueAt(i, 1));
        }
        }
        this.removeAll();
        this.revalidate();
        this.repaint();
        affiche();
    }//GEN-LAST:event_bsupprimerActionPerformed

    private void btnAjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjouterActionPerformed
        this.removeAll();
        this.revalidate();
        this.repaint();
        clean();
        bajoutcomp("AJOUTER UNE CATÉGORIE", null);
    }//GEN-LAST:event_btnAjouterActionPerformed

    public void bajoutcomp(String t, Categorie e){
        lajout.setText(t);
        add(lajout).setBounds(325, 40, 150, 30);
        add(lintitule).setBounds(240, 80, 120, 30);
        add(tintitule).setBounds(400, 80, 180, 30);
        add(lsalaire).setBounds(240, 120, 120, 30);
        add(tsalaire).setBounds(400, 120, 180, 30);
        add(ljour_conge).setBounds(240, 160, 120, 30);
        add(tjour_conge).setBounds(400, 160, 180, 30);
        add(lpenalite).setBounds(240, 200, 120, 30);
        add(tpenalite).setBounds(400, 200, 180, 30);

        if(e!=null){
            id=e.getIdCategotrie();
            tintitule.setText(e.getIntituleCategorie());
            tsalaire.setText(Float.toString(e.getSalaire()));
            tjour_conge.setText(Integer.toString(e.getJourConge()));
            tpenalite.setText(Float.toString(e.getPenalite()));

            add(bmodifier).setBounds(410, 340, 100, 30);
        }
        else 
            add(bcreer).setBounds(410, 340, 100, 30);
        
        add(bback).setBounds(290, 340, 100, 30);
        bback.setBackground(Color.GREEN);
    }
    private void clean() {
        tintitule.setText("");
        tsalaire.setText("");
        tjour_conge.setText("");
        tpenalite.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bsupprimer;
    private javax.swing.JButton btnAjouter;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable templ;
    // End of variables declaration//GEN-END:variables
    private JLabel lajout;
    private JLabel lintitule;
    private JLabel lsalaire;
    private JLabel ljour_conge;
    private JLabel lpenalite;
    private JTextField tintitule;
    private JTextField tsalaire;
    private JTextField tjour_conge;
    private JTextField tpenalite;
    
    private int id;
    private JButton bcreer;
    private JButton bmodifier;
    private JButton bback;

    
}
