/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App.Models;

import java.util.Date;

/**
 *
 * @author EL OMRI
 */
public class Absence {
    private boolean c;
    private int idAbsence;
    private Date Debut;
    private Date Fin;
    private String Motif;
    private String Justificatif;
    private Employe employe;

    public Absence() {
    }

    public Absence(int idAbsence, Date Debut, Date Fin, String Motif, String Justificatif, Employe employe) {
        this.idAbsence = idAbsence;
        this.Debut = Debut;
        this.Fin = Fin;
        this.Motif = Motif;
        this.Justificatif = Justificatif;
        this.employe = employe;
    }

    public Absence(Date Debut, Date Fin, String Motif, String Justificatif, Employe employe) {
        this.Debut = Debut;
        this.Fin = Fin;
        this.Motif = Motif;
        this.Justificatif = Justificatif;
        this.employe = employe;
    }
    
    
    public boolean isC() {
        return c;
    }

    public void setC(boolean c) {
        this.c = c;
    }
    
    public int getidAbsence() {
        return idAbsence;
    }

    public void setidAbsence(int idAbsence) {
        this.idAbsence = idAbsence;
    }

    public Date getDebut() {
        return Debut;
    }

    public void setDebut(Date Debut) {
        this.Debut = Debut;
    }

    public Date getFin() {
        return Fin;
    }

    public void setFin(Date Fin) {
        this.Fin = Fin;
    }

    public String getMotif() {
        return Motif;
    }

    public void setMotif(String Motif) {
        this.Motif = Motif;
    }

    public String getJustificatif() {
        return Justificatif;
    }

    public void setJustificatif(String Justificatif) {
        this.Justificatif = Justificatif;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }
    
    
}
