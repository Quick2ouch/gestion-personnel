/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Tab;

import App.Models.Conge;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author masta
 */
public class CongeTab extends AbstractTableModel{
    String[] titre = new String[]{"","Id", "Nom", "Prenom", "Date début", "Date fin", "Jours restants","Action"};
    
    Vector<Conge> vab = new Vector<Conge>();
    
    @Override
    public int getRowCount() {
        return vab.size();
    }

    @Override
    public int getColumnCount() {
        return titre.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conge ab = vab.get(rowIndex);
        switch(columnIndex){
            case 0 : return ab.isC();
            case 1 : return ab.getIdConge();
            case 2 : return ab.getEmploye();
            case 3 : return ab.getEmploye();
            case 4 : return ab.getDateDebut();
            case 5 : return ab.getDateFin();
            case 6 : return ab.getIdConge();
            case 7 : return "Modifier";
            default:return null;
        }
    }
    
    @Override
    public String getColumnName(int column){
	return titre[column];
    }
    
     @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
      return column == 0;
    }
    
    public void setdata (Vector<Conge> etu){
        vab = new Vector<Conge>();
	for(Conge a:etu){
		vab.add(a);
	}
	fireTableChanged(null);
    }
    
    @Override
    public void setValueAt(Object o, int row, int col) {
         //To change body of generated methods, choose Tools | Templates.
        Conge ce = vab.get(row);
        if (o instanceof Boolean && col == 0) 
            ce.setC((boolean)o);
        fireTableCellUpdated(row, col);
    }
}
