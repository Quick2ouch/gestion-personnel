/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Tab;

import App.Models.Paiement;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Issam
 */
public class PaiementTab extends AbstractTableModel{
    String[] titre = new String[]{"", "Id", "Employé", "Date de Naissance", "Montant", "Action"};
    Vector<Paiement> vpm = new Vector<Paiement>();

    @Override
    public int getRowCount() {
        return vpm.size();
    }

    @Override
    public int getColumnCount() {
        return titre.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Paiement p = vpm.get(rowIndex);
        switch(columnIndex){
            case 0 : return p.isC();
            case 1 : return p.getIdPaiement();
            case 2 : return p.getEmploye();
            case 3 : return p.getDatePaiement();
            case 4 : return p.getMontant();
            case 5 : return "Modifier";
            default:return null;
        }
    }
    
    //A finir ...
    
    public String getColumnName(int column){
	return titre[column];
    }
    
    public void setdata (Vector<Paiement> etu){
        vpm = new Vector<Paiement>();
	for(Paiement a:etu){
		vpm.add(a);
	}
	fireTableChanged(null);
    }
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
      return column == 0;
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
         //To change body of generated methods, choose Tools | Templates.
        Paiement ce = vpm.get(row);
        if (o instanceof Boolean && col == 0) 
            ce.setC((boolean)o);
        fireTableCellUpdated(row, col);
    }
}
