/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Tab;

import App.Models.Categorie;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Hamza
 */
public class CategorieTab  extends AbstractTableModel{
    String[] titre = new String[]{"","Id", "Intitule", "Salaire", "Jour Congé", "Penalité", "Action"};
    Vector<Categorie> vem = new Vector<Categorie>();
    
    @Override
    public int getRowCount() {
        return vem.size();
    }

    @Override
    public int getColumnCount() {
        return titre.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Categorie rs = vem.get(rowIndex);
        switch(columnIndex){
            case 0 : return rs.isC();
            case 1 : return rs.getIdCategotrie();
            case 2 : return rs.getIntituleCategorie();
            case 3 : return rs.getSalaire();
            case 4 : return rs.getJourConge();
            case 5 : return rs.getPenalite();
            case 6 : return "Modifier";
            default:return null;
        }
    }
        
    public String getColumnName(int column){
	return titre[column];
    }
    
    public void setdata (Vector<Categorie> etu){
        vem = new Vector<Categorie>();
	for(Categorie p:etu){
		vem.add(p);
	}
	fireTableChanged(null);
    }
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
      return column == 0;
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
         //To change body of generated methods, choose Tools | Templates.
        Categorie ce = vem.get(row);
        if (o instanceof Boolean && col == 0) 
            ce.setC((boolean)o);
        fireTableCellUpdated(row, col);
    }
}
