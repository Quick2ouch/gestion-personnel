/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package System;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Hamza
 */
public class LogOperation {
    Bootstrap app = Bootstrap.getInstance();
    
    public LogOperation(final String op)
    {
        new Thread() {
            public void run() {                
                try {
                    File fout = new File("log.txt");
                    if(!fout.exists())
                    {
                        fout.createNewFile();
                    }

                    FileOutputStream fos = new FileOutputStream(fout, true);

                    try (OutputStreamWriter osw = new OutputStreamWriter(fos)) {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();

                        String user = app.getStringDataValue("connectedUser");
                        String opDate = dateFormat.format(date);

                        osw.write(opDate+ " ["+user+"]: "+op);
                    }
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }  
        }.start();
        
        
        
    }
    
    
}
