/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import App.Models.Categorie;
import App.ORM.CategorieORM;
import java.util.Vector;

/**
 *
 * @author Naima
 */
public class CategorieTest {

    public CategorieTest() {
    }
    
    public void runTests()
    {
        System.out.println("TESTS CATEGORIE");
        System.out.println("==============================");
        System.out.println("App.ORM.CategorieORM => Find();");
        
        CategorieORM catOrm = new CategorieORM();
        Categorie c = new Categorie();
        Vector<Categorie> vc= new Vector<Categorie>();
        c = catOrm.find(2); 
        System.out.println(c.getIdCategotrie()+" / "+
                c.getIntituleCategorie()+" / "+
                c.getJourConge()+" / "+
                c.getPenalite()+" / "+
                c.getSalaire());
        
        System.out.println("==============================");
        System.out.println("App.ORM.CategorieORM => list();");
        vc = catOrm.list();
        for(int i=0;i<vc.size();i++){
            System.out.println(vc.get(i));
        }
        /*
            //CREATE CATEGORIE:
            System.out.println("==============================");
            System.out.println("App.ORM.CategorieORM => Create();");
            Categorie co1 = new Categorie((float) 7000.0, 60, "ingenieure info", (float) 1000.5);
            catOrm.create(co1);
            
            //UPDATE CATEGORIE
            System.out.println("==============================");
            System.out.println("App.ORM.CategorieORM => Update();");
            Categorie co1 = new Categorie(1,(float) 500.0, 32, "finch", (float) 500.5);
            catOrm.update(co1);
        
            //DELETE CATEGORIE
            System.out.println("==============================");
            System.out.println("App.ORM.CategorieORM => DELETE();");
            catOrm.delete(4);
        */            
    }
}
